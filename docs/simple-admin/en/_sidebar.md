* Basic Configuration
    * [Basic Setting](simple-admin/en/docs/env_setting.md)
    * [Simple Admin Tool](simple-admin/en/docs/simple-admin-tools.md)
    * [File Manager](/simple-admin/en/docs/file_manager.md)
    * [Web Setting](/simple-admin/en/docs/web-setting.md)
    * [Global Variable](/simple-admin/en/docs/global_vars.md)
* Quick Start
    * Develop Core Project
      * [Develop Backend](simple-admin/en/docs/quick_develop_example.md)
      * [Develop Frontend](simple-admin/en/docs/web_develop_example.md)
    * [API Service](simple-admin/en/docs/api_example.md)
    * [RPC Service](simple-admin/en/docs/rpc_example.md)
    * [Docker](simple-admin/en/docs/deploy_docker.md)
* Tools
    * [Validator](/simple-admin/en/docs/validator.md)
    * [Consul](/simple-admin/en/docs/consul.md)
    * [Swagger](simple-admin/en/docs/swagger.md)
    * [GORM](simple-admin/en/docs/gorm.md)
    * [Authorization](simple-admin/en/docs/authorization.md)
    * [Error Handling](simple-admin/en/docs/error_handling.md)
    * [Oauth](simple-admin/en/docs/oauth.md)