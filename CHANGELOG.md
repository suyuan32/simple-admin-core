# 0.0.8 (2022/10/08)

### Features

- docs: add oauth doc
- feat: oauth api service
- feat: oauth rpc service
- docs: update change log

### Bug Fixes

- docs: fix nginx config
- fix: oauth initialize bugs and callback bugs
- fix: add oauth menu insert in initialization code
- fix: change create account rules when use oauth log in
- fix: google oauth

# 0.0.7 (2022/09/30)

### Bug Fixes

- fix: authority tree generation and init database code
- fix: bugs in menu tree generation

# 0.0.7-beta (2022/09/29)

### Features

- feat: dictionary
- feat: dictionary
- docs: add badges
- docs: read only notice
- feat: online preview
- feat: online preview
- docs: deploy in docker
- docs: add consul url
- docs: add consul url
- docs: rpc example service config
- docs: call rpc service
- docs: docker
- docs: docker
- feat: error handling doc
- feat: global variable, GORM and authorization doc
- feat: example doc
- feat: discord link and qq link
- feat: API and RPC example doc
- feat: update changelog

### Bug Fixes

- fix: docker compose and rpc doc
- fix: update tool version

# 0.0.6 (2022/09/23)

### Features

- feat: use consul to do service discover and get configuration feat: dockerfile feat: consul doc

### Bug Fixes

- fix: update change log

# 0.0.5 (2022/09/21)

### Features

- feat: validator doc
- feat: add translation test
- feat: validator

### Bug Fixes

- fix: update simple admin tools version
- fix: add ui build doc
- fix: menu meta setting logic
- fix: use default logger and fix some bugs
- feat: validator definition fix: swagger doc about conditions
- fix: update readme
- fix: update doc
- fix: update readme

# 0.0.4 (2022/09/13)

### Features

- feat: add bilibili link
- feat: web setting doc
- feat: file manager doc
- feat: swagger doc
- feat: example doc

### Bug Fixes

- fix: require properties in api doc
- refactor: menu properties fix: bug in mysql group
- fix: add recommend in doc
- fix: fix the bugs of post request in swagger 修复go-swagger中的post请求参数
- fix: tip for doc
- fix: tip for init
- fix: preview url
- fix: update tool version
- fix: simple admin tool version
- fix: adjust imports for new goctl

# 0.0.2 (2022/09/05)

### Features

- feat: add video link
- feat: add init url
- feat: doc
- feat: user profile

### Bug Fixes

- fix: readme
- fix: optimize readme
- fix: optimize config file
- feat: add logger for all logic fix: some bugs in config
- fix: add user profile init

# 0.0.1 (2022/08/26)

### Features

- feat: swagger doc
- feat: part swagger
- feat: change password
- refactor: optimize project error handler and initialize
- feat: initialize database api
- refactor: reconstruct project

### Bug Fixes

- fix: some bugs on menu and model
- fix: some bugs on user and captcha
- fix: add error msg
- fix: init database context exceed timeout
- fix: optimize error messages management
